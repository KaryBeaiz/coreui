var acumuladoData = {
  labels:["Ene 10,2019","","","","","","","","","","","Dic 10, 2019"],
  datasets:[{
    type: 'line',
    label: 'Porcentaje',
    borderColor: '#1eafed',
    fill: false,
    data: [7500,14500,15800,8800,14900,12400,15000,10000,13650,18000,19840,15000],
    pointRadius: 0
  }, {
    backgroundColor: 'rgba(163,198,220,0.3)',
    data:[9200,13590,14999,13590,10000,16105,14782,11225,19200,14873,10870,17875]
  }]
};

var acumulado = new Chart($('#chart-1'),{
  type: "bar",
  data: acumuladoData,
  options: {
    legend:{
      display: false
    },
    responsive: true,
    tooltips: {
			mode: 'index',
			intersect: false
		}
  }
});
