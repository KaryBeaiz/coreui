var options = {
  circumference: Math.PI,
  rotation: Math.PI,
  cutoutPercentage: 60,
  plugins: {
    datalabels: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
      borderColor: '#ffffff',
      color: function(context) {
        return context.datasets.backgroundColor;
      },
      font: function(context){
        var w = context.chart.width;
        return {
          size: w < 512 ? 18 : 20
        }
      },
      align: 'start',
      anchor: 'start',
      offset: 10,
      borderRadius: 4,
      borderWidth: 1,
      formatter: function(value, context){
        var i = context.dataIndex;
        var len = context.dataset.data.length - 1;
        if(i == len){
          return null;
        }
        return value+'mph';
      }
    }
  },
  legend: {
    display: false
  },
  tooltips: {
    enabled: false
  }
}

var semaforo1 = new Chart($('#chartSemaforo-1'),{
  type: 'doughnut',
  data: {
    datasets: [{
      data: [75,45],
      backgroundColor: ["#39669A","#B0C3C7"]
    }]
  },
  options: options
});

var semaforo2 = new Chart($('#chartSemaforo-2'),{
  type: 'doughnut',
  data: {
    datasets: [{
      data: [90,45],
      backgroundColor: ["#FFB026","#B0C3C7"]
    }]
  },
  options: options
});

var semaforo3 = new Chart($('#chartSemaforo-3'),{
  type: 'doughnut',
  data: {
    datasets: [{
      data: [90,45],
      backgroundColor: ["#FF5151","#B0C3C7"]
    }]
  },
  options: options
});

var semaforo4 = new Chart($('#chartSemaforo-4'),{
  type: 'doughnut',
  data: {
    datasets: [{
      data: [20,95],
      backgroundColor: ["#01A362","#B0C3C7"]
    }]
  },
  options: options
});
