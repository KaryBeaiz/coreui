var chart1Data =  {
  labels: ["Enero","","","Abril","","","","Agosto","","","","Diciembre"],
  datasets: [{
    label: "Acumulado",
    data: [2000, 16000, 20000, 8000, 30000, 1000, 10000, 38000, 6000, 19000, 30000, 14000],
    fill: false,
    borderColor: '#009EFF'
  },{
    label: "Acumulado",
    data: [8000,20000, 24000, 14000, 25000, 1500, 17000, 40000, 9000, 13000, 20000, 18000],
    fill: false,
    borderColor: '#FF5151'
  }]
}

var chart1 = new Chart($('#chart-1'),{
  type: 'line',
  data: chart1Data,
  options: {
    legend:{
      display: false
    },
    responsive: true
  }
});

var chart2Data = {
  labels: ['Linea 1', 'Linea 2', 'Linea 3', 'Linea 4','Linea 5','Linea 6'],
  datasets: [{
    type: 'bar',
    label: 'Dataset 1',
    backgroundColor:'#FFB026',
    data: [15,20,10,5,9,14,0]
  }, {
    type: 'bar',
    label: 'Dataset 2',
    backgroundColor: '#01A362',
    data: [35,10,12,48,24,23,0]
  }]
};

var chart2 = new Chart($('#chart-2'), {
  type: 'bar',
  data: chart2Data,
  options: {
    legend:{
      display: false
    },
    responsive: true,
    tooltips: {
			mode: 'index',
			intersect: false
		},
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }
  }
});
