
var acumuladoData =  {
  labels: ["Enero","","","Abril","","","","Agosto","","","","Diciembre"],
  datasets: [{
    label: "Acumulado",
    data: [2000, 16000, 20000, 8000, 30000, 1000, 10000, 38000, 6000, 19000, 30000, 14000],
    fill: false,
    borderColor: 'rgba(54,181,176,0.7)'
  }]
};
var acumuladoDataDark =  {
  labels: ["Enero","","","Abril","","","","Agosto","","","","Diciembre"],
  datasets: [{
    label: "Acumulado",
    data: [2000, 16000, 20000, 8000, 30000, 1000, 10000, 38000, 6000, 19000, 30000, 14000],
    fill: false,
    borderColor: 'rgba(76,255,248,0.7)'
  }]
}

var barChartData = {
  labels:["Linea 1","Linea 2","Linea 3","Linea 4","Linea 5", "Linea 6"],
  datasets:[
    {
      label: "Bar chart",
      backgroundColor: 'rgba(54,181,176,0.7)',
      hoverBackgroundColor: 'rgba(34,19,107,0.5)',
      data:[110500,92000,72000,53000,28000,8000],
    }
  ]
};
var barChartDataDark = {
  labels:["Linea 1","Linea 2","Linea 3","Linea 4","Linea 5", "Linea 6"],
  datasets:[
    {
      label: "Bar chart",
      backgroundColor: 'rgba(76,255,248,0.7)',
      hoverBackgroundColor: 'rgba(34,19,107,0.5)',
      data:[110500,92000,72000,53000,28000,8000],
    }
  ]
};

var acumulado = new Chart($('#chart-1'),{
  type: 'line',
  data: acumuladoDataDark,
  options: {
    legend:{
      display: false
    },
    responsive: true
  }
});

var barChart = new Chart($('#chart-2'),{
  type: "bar",
  data: barChartDataDark,
  options: {
    legend:{
      display: false
    },
    responsive: true
  }
});
