var oeeData = {
  labels: ['Linea 1', 'Linea 2', 'Linea 3', 'Linea 4','Linea 5','Linea 6'],
  datasets: [{
    type: 'line',
    label: 'Porcentaje',
    borderColor: '#DA0909',
    fill: false,
    data: [70,70,70,70,70,70,70],
    pointRadius: 0
  }, {
    type: 'bar',
    label: 'Dataset 1',
    backgroundColor: [
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(245,139,84)',
      // 'rgba(245,139,84)',
      // 'rgba(245,139,84)'
      'rgba(245,139,84)',
      'rgba(245,139,84)',
      'rgba(245,139,84)',
      'rgba(245,139,84)',
      'rgba(245,139,84)',
      'rgba(245,139,84)'
    ],
    data: [15,35,62,72,80,92,0]
  }, {
    type: 'bar',
    label: 'Dataset 2',
    backgroundColor: 'rgba(232,232,237,0.5)',
    data: [85,65,38,28,20,8,0]
  }]
};
var productionData= {
  labels: ['Linea 1', 'Linea 2', 'Linea 3', 'Linea 4','Linea 5', 'Linea 6'],
  datasets: [{
    type: 'line',
    label: 'Porcentaje',
    borderColor: '#DA0909',
    fill: false,
    data: [70,70,70,70,70,70,70],
    pointRadius: 0
  }, {
    label: 'Dataset 1',
    backgroundColor:  [
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(176,244,230)',
      // 'rgba(176,244,230)',
      'rgba(176,244,230)',
      'rgba(176,244,230)',
      'rgba(176,244,230)',
      'rgba(176,244,230)',
      'rgba(176,244,230)',
      'rgba(176,244,230)'
    ],
    data: [10,22,38,45,75,85,0]
  }, {
    label: 'Dataset 2',
    backgroundColor: 'rgba(227,231,241,0.7)',
    data: [90,78,62,55,25,15,0]
  }]
};
var downtimeData = {
  labels:["Linea 1","Linea 2","Linea 3","Linea 4","Linea 5", "Linea 6"],
  datasets:[{
    type: 'line',
    label: 'Porcentaje',
    borderColor: '#DA0909',
    fill: false,
    data: [20,20,20,20,20,20,20],
    pointRadius: 0
  }, {
    type: 'bar',
    label: "Downtime",
    backgroundColor: [
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(255,211,105)',
      // 'rgba(255,211,105)',
      // 'rgba(255,211,105)',
      'rgba(255,211,105)',
      'rgba(255,211,105)',
      'rgba(255,211,105)',
      'rgba(255,211,105)',
      'rgba(255,211,105)',
      'rgba(255,211,105)',
    ],
    data:[62,45,32,19,10,5,100]
  }]
};
var scrapLineData = {
  labels:["Linea 1","Linea 2","Linea 3","Linea 4","Linea 5","Linea 6"],
  datasets:[{
    type: 'line',
    label: 'Porcentaje',
    borderColor: '#DA0909',
    fill: false,
    data: [20,20,20,20,20,20,20],
    pointRadius: 0
  }, {
    label: "Scrap by Line",
    backgroundColor: [
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(178,34,34,0.8)',
      // 'rgba(18,202,214)',
      // 'rgba(18,202,214)',
      // 'rgba(18,202,214)',
      'rgba(18,202,214)',
      'rgba(18,202,214)',
      'rgba(18,202,214)',
      'rgba(18,202,214)',
      'rgba(18,202,214)',
      'rgba(18,202,214)'
    ],
    data:[68,37,28,20,14,8,100]
  }]
};


var oee = new Chart($('#chart-1'), {
  type: 'bar',
  data: oeeData,
  options: {
    legend:{
      display: false
    },
    responsive: true,
    tooltips: {
			mode: 'index',
			intersect: false
		},
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }
  }
});

var downtime = new Chart($('#chart-2'),{
  type: "bar",
  data: downtimeData,
  options: {
    legend:{
      display: false
    },
    responsive: true,
    tooltips: {
			mode: 'index',
			intersect: false
		}
  }
});

var scrapLine = new Chart($('#chart-3'),{
  type: "bar",
  data: scrapLineData,
  options: {
    legend:{
      display: false
    },
    responsive: true,
    tooltips: {
			mode: 'index',
			intersect: false
		}
  }
});

var production = new Chart($('#chart-4'),{
  type: "bar",
  data: productionData,
  options: {
    legend:{
      display: false
    },
    responsive: true,
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }
  }
});

var downtimeDept = new Chart($('#chart-5'),{
  type: "pie",
  data: {
    labels:["Linea 1","Linea 2","Linea 3","Linea 4"],
    datasets:[
      {
        backgroundColor: ["#6decb9", "#ffd369","#BD033E","#515585"],
        data:[12,45,25,33]
      }
    ]
  },
  options: {
    responsive: true,
    legend:{
      display: false
    }
  }
});

var scrapCause = new Chart($('#chart-6'),{
  type: "pie",
  data: {
    labels:["Linea 1","Linea 2","Linea 3","Linea 4"],
    datasets:[
      {
        backgroundColor: ["#6decb9", "#ffd369","#BD033E","#515585"],
        data:[24,37,43,58]
      }
    ]
  },
  options: {
    responsive: true,
    legend:{
      display: false
    }
  }
});
