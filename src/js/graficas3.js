var produccionData = {
  labels: ['Ene','Feb','Mar','Abr','May','Jun'],
  datasets: [{
    data: [14500,9000,14500,15000,14000,20000],
    backgroundColor: 'rgba(147,42,185,0.1)',
    borderWidth: 0,
    pointBorderColor: 'rgba(147,47,185,0.7)',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 2
  },{
    data: [20000,6000,16000,10500,16500,9500],
    backgroundColor: 'rgba(36,189,210,0.2)',
    borderColor: '#24BDD2',
    borderWidth: 2,
    pointBorderColor: '#24BDD2',
    pointBackgroundColor: '#fff'
  },{
    data: [12000,5000,11100,14000,10500,20000],
    pointRadius: 0,
    fill: false,
    borderColor: 'rgba(147,47,185,0.7)',
    borderWidth: 2
  }]
};
var downtimeLineData = {
  labels: ['Robot','Material','Liberación','Contenedores','Robotos'],
  datasets: [{
    data: [14500,9000,14500,15000,14000,20000],
    backgroundColor: 'rgba(147,42,185,0.1)',
    borderWidth: 0,
    pointBorderColor: 'rgba(147,47,185,0.7)',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 2
  },{
    data: [20000,6000,16000,10500,16500,9500],
    backgroundColor: 'rgba(36,189,210,0.2)',
    borderColor: '#24BDD2',
    borderWidth: 2,
    pointBorderColor: '#24BDD2',
    pointBackgroundColor: '#fff'
  },{
    data: [12000,5000,11100,14000,10500,20000],
    pointRadius: 0,
    fill: false,
    borderColor: 'rgba(147,47,185,0.7)',
    borderWidth: 2
  }]
};

var produccion = new Chart($('#chart-1'),{
  type: 'line',
  data: produccionData,
  options: {
    legend:{
      display: false,
    }
  }
});

var scrap = new Chart($('#chart-2'),{
  type: 'doughnut',
  data: {
    labels: ['Trabajo', 'Puntos frios', 'Fisura', 'Dimensional'],
    datasets: [{
      data: [950,600,768,1054],
      backgroundColor: ["#55D8FE","#FF8373","#FFDA83","#A3A0FB"]
    }]
  },
  options:{
    cutoutPercentage: 55
  }
});

var dowtimeBar = new Chart($('#chart-3'),{
  type: 'bar',
  data:{
    labels: ['Calidad', 'Mantenimiento','Producción','Logística','Ingenería'],
    datasets: [{
       data: [115000,100000,82000,75000,50000,30000,15000,120000],
       backgroundColor: '#40bfc1'
   }]
 },
 options:{
   legend:{
     display: false
   },
   responsive: true
 }
});

var downtimeLine = new Chart($('#chart-4'),{
  type: 'line',
  data: downtimeLineData,
  options: {
    legend:{
      display: false,
    }
  }
});
